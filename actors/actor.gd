class_name Actor
extends CharacterBody2D

## Current knockback to apply to the Actor
var knockback: Vector2 = Vector2.ZERO

## Rotates the Actor to face the given direction
func rotate_actor(direction: Vector2) -> void:
	if direction.x > 0:
		scale = Vector2(1, 1)
		rotation_degrees = 0
	elif direction.x < 0:
		scale = Vector2(1, -1)
		rotation_degrees = 180

## Applies a knockback force to the Actor, regarding a force and a direction
func apply_knockback(knockback_force: float, dir: Vector2) -> void:
	knockback = Vector2(dir.x * knockback_force * 2, dir.y * knockback_force)
