class_name MinerLogic
extends Actor


## Miner's Movement Stats Resource that holds information regarding movement values
@export var miner_movement_stats: SideScrollerMovementStats
## Reference to a StateChart object that maps the Miner's AI
@export var state_chart: StateChart
## Reference to an Aggro component
@export var aggro_input: AggroInput
## Reference to an Attack component
@export var attack_input: AttackInput
## Reference to a Ledge Detector component.
## Detects wether Actor is on a ledge.
@export var ledge_detector: LedgeDetector
## Reference to a Health component
@export var health_component: HealthComponent

## Miner's starting position so it can leash back to it
var starting_position: Vector2
## Last frame's position so the Miner can leash if player is out of range
var last_frame_position: Vector2 = Vector2.ZERO


func _ready() -> void:
	starting_position = global_position
	health_component._on_health_depleted.connect(func(): state_chart.send_event("_on_dying"))
	
	aggro_input._aggroed.connect(func(): state_chart.send_event("_on_aggroed"))
	aggro_input._aggroed.connect(func(): state_chart.send_event("_on_moving"))
	aggro_input._leashed.connect(func(): state_chart.send_event("_on_leashed"))
	aggro_input._leashed.connect(func(): state_chart.send_event("_on_moving"))
	
	attack_input._in_range.connect(func(): state_chart.send_event("_on_attack_range"))
	attack_input._in_range.connect(func(): state_chart.send_event("_on_charging"))
	attack_input._charging_finished.connect(func(): state_chart.send_event("_on_charging_finished"))
	attack_input._attacking_finished.connect(func(): state_chart.send_event("_on_attack_finished"))
	attack_input._recovering_finished.connect(finished_recovering)

"""
	Idle State Logic
"""
func _on_idle_state_entered():
	state_chart.send_event("_on_idle")


func _on_idle_state_physics_processing(_delta: float) -> void:
	add_friction()


"""
	Chasing State Logic
"""
func _on_chasing_event_received(event: StringName) -> void:
	if event == "_on_leashed":
		add_friction()


func _on_chasing_state_physics_processing(_delta: float) -> void:
	if aggro_input.is_aggroed():
		var direction: Vector2 = global_position.direction_to(aggro_input.player.global_position)
		
		if global_position == last_frame_position and not aggro_input.is_leashing():
			aggro_input.start_leashing()
		elif global_position != last_frame_position:
			aggro_input.stop_leashing()
		
		accelerate(direction)
		last_frame_position = global_position
		
		if ledge_detector.is_ledge_right() or ledge_detector.is_ledge_left():
			velocity = Vector2.ZERO


"""
	Retreating State Logic
"""
func _on_retreating_state_physics_processing(_delta: float) -> void:
	if global_position.distance_to(starting_position) < 10:
		state_chart.send_event("_on_starting_position")
	else:
		var direction: Vector2 = global_position.direction_to(starting_position)
		accelerate(direction)


"""
	Dying State Logic
"""
func _on_dying_state_entered() -> void:
	queue_free()


"""
	Charging State Logic
"""
func _on_charging_state_entered():
	if attack_input.is_in_range():
		var attack_direction: Vector2 = global_position.direction_to(attack_input.player.global_position)
		rotate_actor(attack_direction)
	
	attack_input.charge()


func _on_charging_state_physics_processing(_delta: float) -> void:
	add_friction()


"""
	Attacking State Logic
"""
func _on_attacking_state_entered():
	attack_input.attack()


"""
	Recovering State Logic
"""
func _on_recovering_state_entered():
	attack_input.recover()


func finished_recovering() -> void:
	if attack_input.is_in_range():
		state_chart.send_event("_still_on_attack_range")
		state_chart.send_event("_on_charging")
	elif aggro_input.is_aggroed():
		state_chart.send_event("_on_aggroed")
		state_chart.send_event("_on_moving")
	else:
		state_chart.send_event("_on_leashed")
		state_chart.send_event("_on_moving")


"""
	Helper Functions
"""
## Accelerates Actor in a given direction
func accelerate(direction: Vector2) -> void:
	velocity.x = lerp(velocity.x, miner_movement_stats.max_speed * direction.x, miner_movement_stats.move_acceleration)


## Decelerates Actor's velocity
func add_friction() -> void:
	velocity.x = lerp(velocity.x, Vector2.ZERO.x, miner_movement_stats.move_deceleration)
