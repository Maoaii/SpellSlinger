class_name Movement
extends Node

"""
	Export Variables
"""
## The actor that will move
@export var actor: Actor

func _physics_process(_delta: float) -> void:
	actor.move_and_slide()
	actor.velocity += actor.knockback
	actor.knockback = actor.knockback.lerp(Vector2.ZERO, 0.3)
