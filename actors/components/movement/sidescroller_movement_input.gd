class_name SideScrollerMovementInput
extends Node

signal jumping()
signal hit_floor()

@export_category("Component Variables")
## The actor that the movement input will affect
@export var actor: Actor
## Side Scroller Stats Resource that holds the necessary values and
## information regarding movement
@export var sidescroller_movement_stats: SideScrollerMovementStats


## Timer that provides a jumb buffer when jumping
@onready var jump_buffer_timer: Timer = Timer.new()
## Timer that provides a coyote time when leaving a platform
@onready var coyote_timer: Timer = Timer.new()


## Holds information regarding if the Actor was on the floor last frame
var was_on_floor: bool = false
## Holds information regarding if the Actor just pressed the jump input
var jumped: bool = false


var disable_player_input: float = 0.0


func _ready() -> void:
	setup_timers()
	
	if EventBus.disable_player_input:
		EventBus.disable_player_input.connect(func(time): disable_player_input = time)


func _unhandled_input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("jump"):
		jump()
	if Input.is_action_just_released("jump"):
		variable_jump_height()


func _physics_process(delta: float) -> void:
	disable_player_input = maxf(disable_player_input - delta, 0.0)
	var input_dir: Vector2 = input()
	if input_dir != Vector2.ZERO:
		accelerate(input_dir)
		flip_sprite(input_dir)
	elif actor.is_on_floor():
		add_friction(sidescroller_movement_stats.move_deceleration)
	else:
		add_friction(sidescroller_movement_stats.air_deceleration)
	
	if actor.is_on_floor():
		if not jump_buffer_timer.is_stopped():
			jump()
		else:
			jumped = false
		
		if not was_on_floor:
			hit_floor.emit()
		
		was_on_floor = true
	else:
		if was_on_floor and not jumped:
			coyote_timer.start()
		
		was_on_floor = false


## Sets up the component's timers
func setup_timers() -> void:
	jump_buffer_timer.one_shot = true
	coyote_timer.one_shot = true
	
	jump_buffer_timer.wait_time = sidescroller_movement_stats.jump_buffer_time
	coyote_timer.wait_time = sidescroller_movement_stats.coyote_time
	
	add_child(jump_buffer_timer)
	add_child(coyote_timer)


## Flips Acto's sprite depending on movement input
func flip_sprite(dir: Vector2) -> void:
	actor.flip_sprite(true if dir.x > 0 else false)


## Returns a Vector2 that resembles the horizontal movement input, input strength included
func input() -> Vector2:
	if disable_player_input > 0:
		return Vector2.ZERO
	var input_dir: Vector2 = Vector2.ZERO
	
	input_dir.x = Input.get_axis("left", "right")
	input_dir = input_dir.normalized()
	return input_dir


## Accelerates Actor in a given direction
func accelerate(direction: Vector2) -> void:
	actor.velocity.x = lerp(actor.velocity.x, sidescroller_movement_stats.max_speed * direction.x, sidescroller_movement_stats.move_acceleration)


## Decelerates Actor's velocity
func add_friction(weight: float) -> void:
	actor.velocity.x = lerp(actor.velocity.x, Vector2.ZERO.x, weight)


## Makes Actor jump if on floor
func jump() -> void:
	if actor.is_on_floor() or not coyote_timer.is_stopped():
		jumping.emit()
		jumped = true
		actor.velocity.y = sidescroller_movement_stats.jump_velocity
		jump_buffer_timer.stop()
		coyote_timer.stop()
	else:
		jump_buffer_timer.start()


## Limits Actor's jump height when input released
func variable_jump_height() -> void:
	if actor.velocity.y < 0.0:
		actor.velocity.y *= 0.5
