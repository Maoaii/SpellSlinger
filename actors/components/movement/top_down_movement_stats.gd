class_name TopDownMovementStats
extends Resource

@export_category("Movement Variables")
@export var max_speed: float = 700
@export var move_acceleration: float = 80
@export var move_deceleration: float = 120
@export var wall_min_slide_angle: float = 0.0
