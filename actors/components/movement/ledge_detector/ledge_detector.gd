@tool
class_name LedgeDetector
extends Node2D

@export var actor: Actor
@export var detectors_offset: Vector2
@export var detector_transform: Vector2
@onready var left_detector: RayCast2D = $LeftRaycast
@onready var right_detector: RayCast2D = $RightRaycast


func _physics_process(_delta: float) -> void:
	left_detector.target_position = detector_transform
	right_detector.target_position = detector_transform
	
	left_detector.position.x = -detectors_offset.x
	right_detector.position.x = detectors_offset.x
	
	if actor:
		global_position = actor.global_position + Vector2(0, detectors_offset.y)


func is_ledge_left() -> bool:
	return not left_detector.is_colliding()


func is_ledge_right() -> bool:
	return not right_detector.is_colliding()
