class_name GravityMovement
extends Node

## The actor the gravity will apply to
@export var actor: Actor
## Side scrolling stats that holds the information regarding gravity
@export var sidescroller_movement_stats: SideScrollerMovementStats


func _physics_process(delta: float) -> void:
	apply_gravity(delta)


## Applies gravity to Actor
func apply_gravity(delta: float) -> void:
	actor.velocity.y += get_gravity() * delta
	if actor.is_on_floor():
		cap_velocity("y", sidescroller_movement_stats.jump_velocity, 0)


## Return different gravity values depending if Actor is currently ascending or descending
func get_gravity() -> float:
	if actor.is_on_wall_only() and (Input.is_action_pressed("left") or Input.is_action_pressed("right")) and actor.velocity.y > 0.0:
		return sidescroller_movement_stats.wall_slide_gravity
	
	return sidescroller_movement_stats.jump_gravity if actor.velocity.y < 0.0 else sidescroller_movement_stats.fall_gravity


## Clamps an axis's velocity between a minimum and a maximum
func cap_velocity(axis: String, min_velocity: float, capping_velocity: float):
	actor.velocity[axis] = clampf(actor.velocity[axis], min_velocity, capping_velocity)
