class_name TopDownMovementInput
extends Node

@export_category("Component Variables")
## The actor that the movement input will affect
@export var actor: Actor
## Top Down Stats Resource that holds the necessary values and
## information regarding movement
@export var top_down_movement_stats: TopDownMovementStats


func _ready() -> void:
	actor.wall_min_slide_angle = top_down_movement_stats.wall_min_slide_angle
	actor.motion_mode = CharacterBody2D.MOTION_MODE_FLOATING


func _physics_process(delta: float) -> void:
	var input_dir: Vector2 = input()
	
	if input_dir != Vector2.ZERO:
		accelerate(input_dir)
	else:
		add_friction()


## Returns a Vector2 that resembles a 4 directional movement input, with input strength included
func input() -> Vector2:
	return Input.get_vector("left", "right", "up", "down")


## Accelerates Actor in a given direction
func accelerate(input_dir: Vector2) -> void:
	actor.velocity = actor.velocity.move_toward(input_dir * top_down_movement_stats.max_speed, top_down_movement_stats.move_acceleration)


## Decelerates Actor's velocity
func add_friction() -> void:
	actor.velocity = actor.velocity.move_toward(Vector2.ZERO, top_down_movement_stats.move_deceleration)
