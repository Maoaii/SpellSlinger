class_name EntityDashComponent
extends Node2D

signal started_dashing()
signal stopped_dashing()

@export var actor: Actor
@export var velocity: float
@export var duration: float

var direction: Vector2 = Vector2.ZERO
var dashing: bool = false

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("dash"):
		direction = actor.global_position.direction_to(get_global_mouse_position())
		dashing = true
		started_dashing.emit()
		get_tree().create_timer(duration).timeout.connect(stop_dashing)

func _physics_process(_delta: float) -> void:
	if dashing:
		actor.velocity = velocity * direction


func stop_dashing():
	stopped_dashing.emit()
	dashing = false
