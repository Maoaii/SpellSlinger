class_name DoubleJumpComponent
extends Node

@export_category("Component Variables")
@export var actor: Actor
@export var sidescroller_movement_stats: SideScrollerMovementStats

var jumps_available: int

func _ready():
	jumps_available = sidescroller_movement_stats.extra_jumps

func _unhandled_input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("jump"):
		if not actor.is_on_floor() and not actor.is_on_wall() and jumps_available > 0:
			jumps_available -= 1
			jump()

func _physics_process(_delta: float):
	if actor.is_on_floor():
		jumps_available = sidescroller_movement_stats.extra_jumps


## Makes Actor jump if on floor
func jump() -> void:
	actor.velocity.y = sidescroller_movement_stats.jump_velocity
