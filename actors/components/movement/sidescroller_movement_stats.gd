class_name SideScrollerMovementStats
extends Resource

@export_category("Movement Variables")
@export var max_speed: float = 650
@export var move_acceleration: float = 0.75
@export var move_deceleration: float = 0.8
@export var air_deceleration: float = 0.2

@export_category("Jumping Variables")
@export var jump_height: float = 200:
	set(value):
		jump_height = value
		jump_velocity = ((2.0 * jump_height) / jump_time_to_peak) * -1.0
		jump_gravity = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak)) * -1.0
		fall_gravity = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent)) * -1.0

@export var jump_time_to_peak: float = 0.2:
	set(value):
		jump_time_to_peak = value
		jump_velocity = ((2.0 * jump_height) / jump_time_to_peak) * -1.0
		jump_gravity = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak)) * -1.0
		fall_gravity = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent)) * -1.0

@export var jump_time_to_descent: float = 0.4:
	set(value):
		jump_velocity = ((2.0 * jump_height) / jump_time_to_peak) * -1.0
		jump_gravity = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak)) * -1.0
		fall_gravity = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent)) * -1.0

@export var coyote_time: float = 0.2
@export var jump_buffer_time: float = 0.1
@export var terminal_velocity: float = 1000
@export var extra_jumps: int = 0
@export var wall_jump_velocity: float
@export var wall_slide_gravity: float = 200

var jump_velocity : float = ((2.0 * jump_height) / jump_time_to_peak) * -1.0
var jump_gravity : float = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak)) * -1.0
var fall_gravity : float = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent)) * -1.0
