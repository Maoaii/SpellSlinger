class_name WallJumpComponent
extends Node

@export_category("Component Variables")
@export var actor: Actor
@export var sidescroller_movement_stats: SideScrollerMovementStats

var last_wall_normal: Vector2 = Vector2.ZERO

func _unhandled_input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("jump"):
		if actor.is_on_wall_only():
			jump()

func _physics_process(_delta) -> void:
	if actor.is_on_floor():
		last_wall_normal = Vector2.ZERO

func jump() -> void:
	var wall_normal: Vector2 = actor.get_wall_normal()
	if wall_normal == last_wall_normal:
		return
	
	actor.velocity.x = wall_normal.x * abs(sidescroller_movement_stats.wall_jump_velocity)
	actor.velocity.y = sidescroller_movement_stats.jump_velocity * 0.9
	last_wall_normal = wall_normal
	
	EventBus.disable_player_input.emit(0.1)
