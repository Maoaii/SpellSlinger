class_name ManaComponent
extends Node

##
signal _on_mana_changed(new_mana: int)
##
signal _on_mana_depleted()

##
@export var max_mana: int
##
@export var player_stats: String
##
@export var mana_debug_label: Label

@export var mana_bar: ProgressBar

var current_mana: int
var mana_replenish_timer: Timer = Timer.new()

func _ready() -> void:
	current_mana = max_mana
	mana_replenish_timer.wait_time = 1
	mana_replenish_timer.one_shot = false
	mana_replenish_timer.timeout.connect(func(): replenish_mana(1))
	add_child(mana_replenish_timer)
	mana_replenish_timer.start()
	
	if mana_bar:
		mana_bar.max_value = max_mana
		mana_bar.value = max_mana

func _process(_delta: float) -> void:
	if mana_debug_label:
		mana_debug_label.text = "Mana: " + str(current_mana)

func has_enough_mana(mana_needed: int) -> bool:
	return current_mana >= mana_needed

func use_mana(mana_used: int) -> void:
	if current_mana == 0:
		return
	
	current_mana = max(0, current_mana - mana_used)
	mana_bar.value = current_mana
	_on_mana_changed.emit()
	
	if current_mana == 0:
		_on_mana_depleted.emit()


func replenish_mana(amount_replenished: int) -> void:
	if current_mana == max_mana:
		return
	
	current_mana = min(max_mana, current_mana + amount_replenished)
	mana_bar.value = current_mana
	_on_mana_changed.emit()


func full_replenish_mana() -> void:
	current_mana = max_mana
	mana_bar.value = current_mana
	_on_mana_changed.emit()
