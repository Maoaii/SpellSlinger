class_name ShootingInput
extends Node2D

## Signal fired whenever a shooting input is registered and a bullet is shot
signal _on_weapon_shot()
## Signal fired whenever the current ammo is depleted
signal _on_ammo_depleted()
## Signal fired whenever a reload input is registered and the magazine's ammo is replenished
signal _on_reload()

## This component's states
enum ShootingStates {
	IDLE,
	
	# Shooting states
	CHARGING,
	CHARGED,
	RECOVERING,
	RELOADING,
	
	# Spell states
	SPELL_CHARGING,
	SPELL_CHARGED,
	SPELL_RECOVERING,
}

## The actor from which shooting will come from
@export var actor: Actor
## Marker from which the bullets will spawn from
@export var spawn_marker: Marker2D
## Shooting Stats Resource that holds all the information regarding a weapon
@export var shooting_stats: ShootingStats
## Label that shows the current state of this component (optional)
@export var debugger_label: Label
## Hotbar that holds the spells the Actor has access to
@export var hotbar: Hotbar
##
@export var spells: Array[SpellResource]
##
@export var mana_component: ManaComponent

@export var ammo_bar: TextureProgressBar
##
@export var trajectory_line: TrajectoryLine

## Timer that limits the rate of fire of this weapon
@onready var fire_rate_timer: Timer = Timer.new()
## Timer that limits the amount of time needed to charge this weapon
@onready var charge_timer: Timer = Timer.new()
## Timer that limits the reload time of this weapon
@onready var reload_timer: Timer = Timer.new()

## Current weapon's ammo
var weapon_ammo: int
## Current component state
var current_state: ShootingStates = ShootingStates.IDLE
##
var current_selected_spell: int = -1

func _ready() -> void:
	weapon_ammo = shooting_stats.ammo
	if ammo_bar:
		ammo_bar.max_value = shooting_stats.ammo
		ammo_bar.value = shooting_stats.ammo
	setup_timers()
	if hotbar:
		hotbar.update_spell_containers(spells)

func _unhandled_input(_event: InputEvent) -> void:
	process_spell_input()

func update_trajectory_line(delta: float) -> void:
	if current_selected_spell != -1:
		var direction = spawn_marker.global_position.direction_to(get_global_mouse_position())
		var spell_stats: SpellStats = spells[current_selected_spell].spell_stats
		trajectory_line.update_trajectory(direction, spell_stats.speed, spell_stats.weight, spawn_marker.global_position, delta)
			
	elif current_state == ShootingStates.CHARGING or current_state == ShootingStates.CHARGED:
		var direction = spawn_marker.global_position.direction_to(get_global_mouse_position())
		trajectory_line.update_trajectory(direction, shooting_stats.bullet_speed/3, shooting_stats.bullet_weight, spawn_marker.global_position, delta)
		
	else:
		trajectory_line.clear_points()

func _physics_process(delta: float) -> void:
	if debugger_label:
		show_debug_information()
	
	if trajectory_line:
		update_trajectory_line(delta)
	
	if Input.is_action_just_pressed("reload"):
		current_state = ShootingStates.RELOADING
		reload_timer.start()
		if ammo_bar:
			var tween = get_tree().create_tween()
			tween.tween_property(ammo_bar, "value", shooting_stats.ammo, reload_timer.wait_time)
	
	if Input.is_action_pressed("shoot") and current_state == ShootingStates.IDLE:
		if weapon_ammo == 0:
			current_state = ShootingStates.RELOADING
			reload_timer.start()
			if ammo_bar:
				var tween = get_tree().create_tween()
				tween.tween_property(ammo_bar, "value", shooting_stats.ammo, reload_timer.wait_time)
		else:
			current_state = ShootingStates.CHARGING
			EventBus._on_player_charging.emit()
			charge_timer.start()
	
	if Input.is_action_just_released("shoot"):
		EventBus._on_player_shot.emit()
		if current_state == ShootingStates.CHARGED:
			shoot()
		elif current_state == ShootingStates.CHARGING:
			current_state = ShootingStates.IDLE
			charge_timer.stop()


## Sets up the timers for all the weapon's mechanics
func setup_timers() -> void:
	fire_rate_timer.wait_time = shooting_stats.fire_rate
	fire_rate_timer.one_shot = true
	fire_rate_timer.timeout.connect(func(): current_state = ShootingStates.IDLE)
	add_child(fire_rate_timer)
	
	charge_timer.wait_time = shooting_stats.charge_time
	charge_timer.one_shot = true
	if shooting_stats.firing_mode == shooting_stats.FiringMode.Auto:
		charge_timer.timeout.connect(shoot)
	elif shooting_stats.firing_mode == shooting_stats.FiringMode.Single:
		charge_timer.timeout.connect(func(): current_state = ShootingStates.CHARGED)
	add_child(charge_timer)
	
	reload_timer.wait_time = shooting_stats.reload_time
	reload_timer.one_shot = true
	reload_timer.timeout.connect(reload)
	add_child(reload_timer)


## Writes the current shooting state to the debugger label, if applicable
func show_debug_information() -> void:
	if debugger_label:
		debugger_label.text = str(ShootingStates.keys()[current_state])


## Reloads the weapon's ammo fully and emits _on_reload signal
func reload():
	current_state = ShootingStates.IDLE
	weapon_ammo = shooting_stats.ammo
	if ammo_bar:
		ammo_bar.value = weapon_ammo
	_on_reload.emit()


## Spawns a new bullet and sends it to the mouse's position on trigger.
## Also spends the weapon's ammo and applies a knockback to the Actor
func shoot() -> void:
	current_state = ShootingStates.RECOVERING
	fire_rate_timer.start()
	
	var direction: Vector2 = actor.global_position.direction_to(get_global_mouse_position())
	match shooting_stats.shooting_type:
		shooting_stats.ShootingType.LINEAR:
			var new_angle = deg_to_rad(randf_range(-shooting_stats.exposed_properties["Shooting Type Variables/Max Spread Angle"], shooting_stats.exposed_properties["Shooting Type Variables/Max Spread Angle"]))
			
			direction = Vector2.from_angle(direction.angle() + new_angle)
			
			spawn_bullet(spawn_marker.global_position, direction, shooting_stats.bullet_speed, shooting_stats.bullet_damage, shooting_stats.bullet_weight)
		
		shooting_stats.ShootingType.CONIC:
			var amount_of_bullets: int = shooting_stats.exposed_properties["Shooting Type Variables/Number of Bullets"]
			
			for i in range(1, (amount_of_bullets / 2) + 1) \
					if amount_of_bullets % 2 == 0 \
					else range(0, ceil(amount_of_bullets / 2)):
				
				var angle: float = shooting_stats.exposed_properties["Shooting Type Variables/Angle Between Shots"] * i
				var new_direction = direction.rotated(deg_to_rad(angle))
				
				spawn_bullet(spawn_marker.global_position, new_direction, shooting_stats.bullet_speed, shooting_stats.bullet_damage, shooting_stats.bullet_weight)
				
				if i > 0:
					var simetric_direction = direction.rotated(deg_to_rad(angle * -1))
					spawn_bullet(spawn_marker.global_position, simetric_direction, shooting_stats.bullet_speed, shooting_stats.bullet_damage, shooting_stats.bullet_weight)
	
	actor.apply_knockback(shooting_stats.shoot_knockback_force, direction * -1)
	spend_ammo()

## Spawns a bullet scence and gives it speed and direction
func spawn_bullet(pos: Vector2, dir: Vector2, speed: float, bullet_damage: float, bullet_weight: float) -> void:
	var bullet: Bullet = shooting_stats.bullet_scene.instantiate()
	bullet.init_variables(pos, dir, speed, bullet_damage, bullet_weight)
	add_child(bullet)

## Removes weapon's ammo from magazine.
## Emits _on_weapon_shot signal every time, and _on_ammo_depleted whenever the current ammo is depleted
func spend_ammo() -> void:
	weapon_ammo = max(0, weapon_ammo - shooting_stats.shoot_ammo)
	if ammo_bar:
		ammo_bar.value = weapon_ammo
	_on_weapon_shot.emit()
	
	if weapon_ammo == 0:
		_on_ammo_depleted.emit()

func can_cast_spell(index: int):
	return current_state == ShootingStates.IDLE and mana_component.has_enough_mana(spells[index].spell_stats.mana_cost) and hotbar.is_spell_ready(index)

func start_casting_spell(_index):
	current_state = ShootingStates.SPELL_CHARGING
	get_tree().create_timer(spells[current_selected_spell].spell_stats.charge_time).timeout.connect( \
		func(): \
			if current_state == ShootingStates.SPELL_CHARGING: current_state = ShootingStates.SPELL_CHARGED)

func process_spell_input() -> void:
	if Input.is_action_pressed("spell1") and can_cast_spell(0):
		current_selected_spell = 0
		start_casting_spell(current_selected_spell)
		EventBus._on_spell_selected.emit(0)
	
	if Input.is_action_just_released("spell1"): 
		current_selected_spell = -1
		EventBus._on_spell_selected.emit(-1)
		current_state = ShootingStates.IDLE if (current_state == ShootingStates.SPELL_CHARGED or \
			current_state == ShootingStates.SPELL_CHARGING) else current_state
	
	if Input.is_action_just_pressed("shoot") and \
			hotbar.is_spell_ready(current_selected_spell) and \
			current_state == ShootingStates.SPELL_CHARGED:
		hotbar.start_casting_spell(current_selected_spell)
		mana_component.use_mana(spells[current_selected_spell].spell_stats.mana_cost)

		if spells[current_selected_spell].spell_type == "Projectile":
			var direction: Vector2 = actor.global_position.direction_to(get_global_mouse_position())
			var spell_stats: SpellStats = spells[current_selected_spell].spell_stats
			var spell: Bullet = spells[current_selected_spell].spell_scene.instantiate() 
			spell.init_variables(spawn_marker.global_position, direction, spell_stats.speed, spell_stats.damage, spell_stats.weight)
			add_child(spell)
		
		current_selected_spell = -1
		current_state = ShootingStates.SPELL_RECOVERING
		EventBus._on_spell_cast.emit(current_selected_spell)
		get_tree().create_timer(0.1).timeout.connect(func(): current_state = ShootingStates.IDLE)
