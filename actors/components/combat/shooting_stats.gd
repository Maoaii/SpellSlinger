@tool
class_name ShootingStats
extends Resource

@export_group("Bullet Variables")
## The damage a single bullet from this weapons this does
@export var bullet_damage: float

## Travel speed of a single bullet fired from this weapon
@export var bullet_speed: float

## Texture of the bullet projectile
@export var bullet_scene: PackedScene

#
@export var bullet_weight: float

@export_group("Shooting Variables")
## Amount of ammo available in a single "magazine"
@export var ammo: int

## Shots used when firing the weapon
@export var shoot_ammo: int

## The time it takes, in seconds, to charge a single shot from this weapon
@export var charge_time: float

## The time it takes, in seconds, to reload a full "magazine"
@export var reload_time: float

## The time it takes, in seconds, to be able to shoot another bullet in the "magazine"
@export var fire_rate: float

## The force applied to the entity getting hit by a bullet
@export var bullet_knockback_force: float
enum FiringMode {
	Auto,
	Single,
}

@export var firing_mode: FiringMode = FiringMode.Auto

enum ShootingType {
	## Shoots bullet in a straight line
	LINEAR, 
	## Shoots bullets in a cone, like a shotgun
	CONIC,
}


## Weapon shooting mode
@export var shooting_type: ShootingType = ShootingType.LINEAR:
	set(value):
		shooting_type = value
		notify_property_list_changed()

var exposed_properties: Dictionary = {
	"Shooting Type Variables/Max Spread Angle": 0,
	"Shooting Type Variables/Number of Bullets": 0,
	"Shooting Type Variables/Angle Between Shots": 0,
}

func _set(property, value) -> bool:
	if property in exposed_properties:
		exposed_properties[property] = value
		return true
	
	return false

func _get(property):
	if property in exposed_properties:
		return exposed_properties[property]
	
	return null

func _get_property_list() -> Array[Dictionary]:
	var list: Array[Dictionary] = []
	
	if shooting_type == ShootingType.LINEAR:
		list.append({
			"name": "Shooting Type Variables/Max Spread Angle",
			"type": TYPE_FLOAT
		})
	elif shooting_type == ShootingType.CONIC:
		list.append({
			"name": "Shooting Type Variables/Number of Bullets",
			"type": TYPE_INT
		})
		list.append({
			"name": "Shooting Type Variables/Angle Between Shots",
			"type": TYPE_FLOAT
		})
	
	return list


## The force applied to the weapon's wielder after firing
@export var shoot_knockback_force: float
