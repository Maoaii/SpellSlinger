@tool
class_name AggroInput
extends Area2D

## Signal fired whenever the player gets in range of the aggro range
signal _aggroed()
## Signal fired whenever the player leaves the aggro range
signal _leashed()


## Actor that will be aggroed
@export var actor: Actor
## The size of the collision shape that will detect hurt collisions
@export var collision_shape_radius: float


@onready var collision_shape: CollisionShape2D = $CollisionShape2D

## Reference to player so we can signal if player's in range
var player: Player = null
## Timer responsible for leash time on an actor
var leash_timer: Timer = Timer.new()


func _ready() -> void:
	leash_timer.wait_time = 1
	leash_timer.one_shot = true
	leash_timer.name = "Leash Timer"
	leash_timer.timeout.connect(func(): _leashed.emit())
	add_child(leash_timer)


func _process(_delta: float) -> void:
	collision_shape.shape.radius = collision_shape_radius
	if actor:
		global_position = actor.global_position


## Starts a leashing timer
func start_leashing() -> void:
	leash_timer.start()


## Stops the leashing timer
func stop_leashing() -> void:
	leash_timer.stop()


## Returns true if the leashing timer is counting
func is_leashing() -> bool:
	return not leash_timer.is_stopped()


## Returns true if player is withing aggro range
func is_aggroed() -> bool:
	return player != null


func _on_body_entered(body):
	player = body
	_aggroed.emit()


func _on_body_exited(_body):
	player = null
	_leashed.emit()
