class_name MeleeAttackStats
extends Resource

@export var charge_time: float
@export var attack_time: float
@export var recover_time: float

@export var attack_damage: int
