@tool
class_name AttackInput
extends Area2D


## Signal fired whenever the player is within attack range
signal _in_range()
## Signal fired whenever the player leaves the attack range
signal _out_of_range()
## Signal fired when attack is finished charging
signal _charging_finished()
## Signal fired when attack is finished
signal _attacking_finished()
## Signal fired when recovery time has finished
signal _recovering_finished()


## Actor that will attack
@export var actor: Actor
## The size of the collision shape that will detect the attack range
@export var collision_shape_radius: float
## Melee Attack Stats tha hold information regarding this enemy's attack stats
@export var enemy_attack_stats: MeleeAttackStats



@onready var collision_shape: CollisionShape2D = $CollisionShape2D


## Timer responsible for limiting the enemy's charge time
var charging_timer: Timer = Timer.new()
## Timer responsible for limiting the enemy's attack time
var attacking_timer: Timer = Timer.new()
## Timer responsible for limiting the enemy's recovery time
var recovering_timer: Timer = Timer.new()
## Reference to the player in order to know if player's within attacking range
var player: Player = null


func _ready() -> void:
	charging_timer.wait_time = enemy_attack_stats.charge_time
	charging_timer.one_shot = true
	charging_timer.name = "Charging Timer"
	charging_timer.timeout.connect(func(): _charging_finished.emit())
	add_child(charging_timer)
	
	attacking_timer.wait_time = enemy_attack_stats.attack_time
	attacking_timer.one_shot = true
	attacking_timer.name = "Attack Timer"
	attacking_timer.timeout.connect(func(): _attacking_finished.emit())
	add_child(attacking_timer)
	
	recovering_timer.wait_time = enemy_attack_stats.recover_time
	recovering_timer.one_shot = true
	recovering_timer.name = "Recover Timer"
	recovering_timer.timeout.connect(func(): _recovering_finished.emit())
	add_child(recovering_timer)


func _process(_delta: float) -> void:
	collision_shape.shape.radius = collision_shape_radius
	if actor:
		global_position = actor.global_position

## Starts charging attack
func charge() -> void:
	charging_timer.start()


## Starts attack
func attack() -> void:
	attacking_timer.start()


## Starts recovery timer
func recover() -> void:
	recovering_timer.start()


## Returns true if player is within attack range
func is_in_range() -> bool:
	return player != null


func _on_body_entered(body):
	player = body
	_in_range.emit()


func _on_body_exited(_body):
	player = null
	_out_of_range.emit()
