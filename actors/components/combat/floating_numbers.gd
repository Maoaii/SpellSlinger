class_name FloatingNumbers
extends Marker2D

## Sets this label's value with an integer
func set_label(value: int):
	$Label.text = str(value)
