class_name HealthComponent
extends Node

## Signal fired whenever this component's current health is changed
## (healed or damaged)
signal _on_health_changed(new_health: int, health_change: int)
## Signal fired when this component's current health is depleted
signal _on_health_depleted()


## The maximum health attributed to the component.
## Componen't current health is set to max_health on load.
@export var max_health: int
## The component responsible for registering damage taken
@export var hurt_input: HurtInput

@export var health_bar: ProgressBar
@export var health_debug_label: Label

@export var invincibility_time: float

var current_health: int
var invincibility_timer: Timer = Timer.new()

func _ready() -> void:
	invincibility_timer.wait_time = invincibility_time
	invincibility_timer.one_shot = true
	add_child(invincibility_timer)
	
	current_health = max_health
	hurt_input._on_damage_taken.connect(take_damage)
	
	if health_bar:
		health_bar.max_value = max_health
		health_bar.value = max_health


func _process(_delta: float) -> void:
	if health_debug_label:
		health_debug_label.text = str(current_health)


## Reduces this component's current health in response to the damage_taken parameter.
## Emits _on_health_changed signal every time, and _on_health_depleted signal 
## once health reaches 0.
func take_damage(damage_taken: int) -> void:
	if current_health == 0 or not invincibility_timer.is_stopped():
		return
	
	current_health = max(0, current_health - damage_taken)
	if health_bar:
		health_bar.value = current_health
	_on_health_changed.emit(current_health, damage_taken)
	invincibility_timer.start()
	
	if current_health == 0:
		_on_health_depleted.emit()

## Increases this component's current health in response to the amount_healed parameter.
## Emits _on_health_changed signal
func heal(amount_healed: int) -> void:
	if current_health == max_health:
		return
	
	current_health = min(max_health, current_health + amount_healed)
	if health_bar:
		health_bar.value = current_health
	_on_health_changed.emit(current_health, amount_healed)

## Fully heals this component's current health.
## Emits _on_health_changed signal
func full_heal() -> void:
	_on_health_changed.emit(current_health, max_health - current_health)
	current_health = max_health
	if health_bar:
		health_bar.value = current_health
