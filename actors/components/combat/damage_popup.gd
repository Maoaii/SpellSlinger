@tool
class_name DamagePopup
extends Node2D

## The actor from which the damage popup will appear from
@export var actor: Actor
## Component that signals when the actor takes damage
@export var health_component: HealthComponent
@export var debug_circle: bool = true
## Offset from which the damage popup will spawn from
@export var offset: Vector2 = Vector2.ZERO
## The Scene that will be spawned when Actor is damaged
@export var damage_node: PackedScene


func _ready() -> void:
	if health_component:
		health_component._on_health_changed.connect(popup)
	randomize()


func _process(_delta: float) -> void:
	if actor:
		global_position = actor.global_position
	
	queue_redraw()


func _draw() -> void:
	if debug_circle:
		draw_circle(offset, 10, Color.WHITE)


## Spawns and tweens a scene that shows the amount of damage taken
func popup(_new_health: int, damage_taken: int) -> void:
	var damage = damage_node.instantiate()
	damage.set_label(damage_taken)
	damage.global_position += offset
	
	var tween = get_tree().create_tween()
	tween.tween_property(damage,
						"position",
						damage.global_position + get_direction(),
						0.75)
	
	
	add_child(damage)


## Returns a random direction upwards
func get_direction() -> Vector2:
	return Vector2(randf_range(-1, 1), -randf()) * 16
