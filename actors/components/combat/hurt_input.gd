@tool
class_name HurtInput
extends Area2D

## Signal fired whenever a collision that should hurt Actor is detected
signal _on_damage_taken(damage)

## The actor from which the hurt collisions will be detected from
@export var actor: Actor
## The size of the collision shape that will detect hurt collisions
@export var collision_shape_size: Vector2

@onready var collision_shape: CollisionShape2D = $CollisionShape2D

func _physics_process(_delta: float) -> void:
	collision_shape.shape.size = collision_shape_size
	if actor:
		global_position = actor.global_position


func _on_area_entered(area):
	if area.has_method("get_damage"):
		_on_damage_taken.emit(area.get_damage())
	elif area.get_parent().has_method("get_damage"):
		_on_damage_taken.emit(area.get_parent().get_damage())


func _on_body_entered(_body):
	pass
