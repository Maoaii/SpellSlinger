@tool
class_name AttackArea
extends Area2D

@export var actor: Actor
@export var melee_attack_stats: MeleeAttackStats
## The size of the collision shape that will detect hurt collisions
@export var collision_shape_size: Vector2
@export var offset: Vector2

@onready var collision_shape: CollisionShape2D = $CollisionShape2D


func _physics_process(_delta: float) -> void:
	collision_shape.shape.size = collision_shape_size
	if actor:
		global_position = actor.global_position
		collision_shape.position = offset


func get_damage() -> int:
	return melee_attack_stats.attack_damage
