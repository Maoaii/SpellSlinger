class_name DashSprite
extends Node2D
## haha
## 

@export var dash_component: EntityDashComponent
@export var actor: Actor
@export var sprite: Texture2D
@export var dash_sprite_spawn_rate: float = 0.15

var dash_timer: Timer = Timer.new()

func _ready() -> void:
	dash_component.started_dashing.connect(start_dashing)
	dash_component.stopped_dashing.connect(stop_dashing)
	
	dash_timer.one_shot = false
	dash_timer.autostart = false
	dash_timer.wait_time = dash_sprite_spawn_rate
	dash_timer.timeout.connect(ghosting)
	add_child(dash_timer)

func start_dashing() -> void:
	ghosting()
	dash_timer.start()

func stop_dashing() -> void:
	dash_timer.stop()

func ghosting():
	var new_sprite = Sprite2D.new()
	new_sprite.texture = sprite
	new_sprite.scale = Vector2(0.5, 0.5)
	new_sprite.global_position = actor.global_position
	new_sprite.show_behind_parent = true
	new_sprite.self_modulate = Color(1, 1, 1, 0.7)
	new_sprite.region_enabled = true
	new_sprite.region_rect = Rect2(32, 0, 224, 416)
	if actor.velocity.x > 0:
		new_sprite.flip_h = true
	else:
		new_sprite.flip_h = false
	add_child(new_sprite)
	
	var tween_fade = get_tree().create_tween()
	
	tween_fade.tween_property(new_sprite, "self_modulate", Color(1, 1, 1, 0), 0.75)
	await tween_fade.finished
	
	new_sprite.queue_free()
