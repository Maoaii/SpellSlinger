class_name StretchAndSquash
extends Node

enum EASINGS {
	EASE_IN = 0,
	EASE_OUT = 1,
	EASE_IN_OUT = 2,
	EASE_OUT_IN = 3
}

enum TRANSITION_TYPES {
	LINEAR = 0,
	SINE = 1,
	QUINT = 2,
	QUART = 3,
	QUAD = 4,
	EXPO = 5,
	ELASTIC = 6,
	CUBIC = 7,
	CIRC = 8,
	BOUNCE = 9,
	BACK = 10,
	SPRING = 11
}

@export var movement: SideScrollerMovementInput
@export var sprite: Sprite2D

@export_category("Reset")
@export var scale_reset_speed: float = 1.0
@export var reset_ease: EASINGS
@export var reset_transition: TRANSITION_TYPES

@export_category("Stretch")
@export var has_stretch: bool = true
@export var stretch_amount: Vector2
@export var stretch_speed: float = 1.0
@export var stretch_ease: EASINGS
@export var stretch_transition: TRANSITION_TYPES

@export_category("Squash")
@export var has_squash: bool = true
@export var squash_amount: Vector2
@export var squash_speed: float = 1.0
@export var squash_ease: EASINGS
@export var squash_transition: TRANSITION_TYPES

var initial_sprite_scale: Vector2


func _ready() -> void:
	initial_sprite_scale = sprite.scale
	if has_stretch:
		movement.jumping.connect(stretch)
	
	if has_squash:
		movement.hit_floor.connect(squash)


func stretch() -> void:
	var tween = get_tree().create_tween().\
		set_ease(get_ease_type(stretch_ease)).\
		set_trans(get_transition_type(stretch_transition))
	tween.finished.connect(reset_sprite)
	tween.tween_property(sprite, "scale", initial_sprite_scale + stretch_amount, stretch_speed)


func reset_sprite() -> void:
	var tween = get_tree().create_tween().\
		set_ease(get_ease_type(reset_ease)).\
		set_trans(get_transition_type(reset_transition))
	tween.tween_property(sprite, "scale", initial_sprite_scale, scale_reset_speed)


func squash() -> void:
	var tween = get_tree().create_tween().\
		set_ease(get_ease_type(squash_ease)).\
		set_trans(get_transition_type(squash_transition))
	tween.finished.connect(reset_sprite)
	tween.tween_property(sprite, "scale", initial_sprite_scale + squash_amount, squash_speed)


func get_ease_type(local_ease: EASINGS):
	match local_ease:
		Tween.EASE_IN:
			return Tween.EASE_IN

		Tween.EASE_OUT:
			return Tween.EASE_OUT

		Tween.EASE_IN_OUT:
			return Tween.EASE_IN_OUT

		Tween.EASE_OUT_IN:
			return Tween.EASE_OUT_IN


func get_transition_type(local_transition: TRANSITION_TYPES):
	match local_transition:
		Tween.TRANS_LINEAR:
			return Tween.TRANS_LINEAR

		Tween.TRANS_SINE:
			return Tween.TRANS_SINE

		Tween.TRANS_QUINT:
			return Tween.TRANS_QUINT

		Tween.TRANS_QUART:
			return Tween.TRANS_QUART

		Tween.TRANS_QUAD:
			return Tween.TRANS_QUAD

		Tween.TRANS_EXPO:
			return Tween.TRANS_EXPO

		Tween.TRANS_ELASTIC:
			return Tween.TRANS_ELASTIC

		Tween.TRANS_CUBIC:
			return Tween.TRANS_CUBIC

		Tween.TRANS_CIRC:
			return Tween.TRANS_CIRC

		Tween.TRANS_BOUNCE:
			return Tween.TRANS_BOUNCE

		Tween.TRANS_BACK:
			return Tween.TRANS_BACK

		Tween.TRANS_SPRING:
			return Tween.TRANS_SPRING
