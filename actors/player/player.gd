class_name Player
extends Actor

## Marker from which shooting will come from
@onready var sling_tip: Marker2D = $SlingTip
## Actor's sprite
@onready var sprite: Sprite2D = $Sprite2D


func _physics_process(_delta: float) -> void:
	var new_marker_pos: Vector2 = (get_global_mouse_position() - global_position).limit_length(100)
	sling_tip.position = new_marker_pos


## Returns the shooting Marker global position
func get_shooting_position() -> Vector2:
	return sling_tip.global_position


## Flips the Actor's sprite on the horizontal axis
func flip_sprite(flip_status: bool) -> void:
	sprite.flip_h = flip_status


