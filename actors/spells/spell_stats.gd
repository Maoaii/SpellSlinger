class_name SpellStats
extends Resource

@export var speed: float
@export var weight: float
@export var charge_time: float
@export var cooldown_time: float
@export var damage: int
@export var mana_cost: int
