class_name TrajectoryLine
extends Line2D

@export var max_points: int

func update_trajectory(dir: Vector2, speed: float, gravity: float, pos: Vector2, delta: float) -> void:
	clear_points()
	var vel = dir * speed
	for i in max_points:
		add_point(pos)
		vel.y += gravity * delta
		pos += vel * delta
