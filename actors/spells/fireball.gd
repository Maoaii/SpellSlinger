extends Bullet


@onready var explosion_hitbox: Area2D = $ExplostionHitbox

func _on_travelling_hitbox_body_entered(_body):
	if traveling_particles:
		traveling_particles.emitting = false
	if hit_particles:
		for particle_effect in hit_particles:
			particle_effect.emitting = true
	velocity = Vector2.ZERO

	if hit_particles.size() > 0:
		hit_particles[hit_particles.size() - 1].finished.connect(func(): queue_free())
	$TravellingHitbox/CollisionShape2D.set_deferred("disabled", true)
	
	explosion_hitbox.get_child(0).set_deferred("disabled", false)
	moving = false
