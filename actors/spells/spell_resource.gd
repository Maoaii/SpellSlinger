class_name SpellResource
extends Resource

@export_enum("Offensive", "Defensive", "Utility")
var spell_category = "Offensive"
@export_enum("Projectile", "Self")
var spell_type = "Projectile"
@export var name: String
@export_multiline var description: String
@export var icon: Texture2D 
@export var spell_scene: PackedScene
@export var player_effects: Dictionary
@export var spell_stats: SpellStats
