class_name Bullet
extends CharacterBody2D

@export var traveling_particles: GPUParticles2D
@export var hit_particles: Array[GPUParticles2D]

## This bullet's damage
var bullet_damage: float = 0
var bullet_speed: float
var bullet_gravity: float
var bullet_velocity: Vector2
var moving: bool = true



func _ready() -> void:
	if traveling_particles:
		traveling_particles.emitting = true


## Initializes this bullet's variables
func init_variables(pos: Vector2, direction: Vector2, speed: float, damage: float, weight: float):
	global_position = pos
	bullet_gravity = weight
	bullet_speed = speed
	bullet_damage = damage

	bullet_velocity = speed * direction
	velocity = bullet_velocity


func _physics_process(delta) -> void:
	velocity.y += bullet_gravity * delta
	if moving:
		move_and_slide()


func _on_visible_on_screen_notifier_2d_screen_exited():
	get_tree().create_timer(3).timeout.connect(func(): queue_free())


func _on_area_2d_body_entered(_body):
	queue_free()


## Returns this bullet's damage
func get_damage() -> float:
	return bullet_damage
