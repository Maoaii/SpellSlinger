extends Camera2D

## The max distance the camera will pan when charging an attack
@export var max_pan_strength: float = 200.0
## The max shake strength the camera will go through when charging an attack
@export var max_shake_strength: float = 3.0
## The amount of shake that increases by unit of time
@export var shake_strength_increase: float = 0.5


## RNG generator
var rng = RandomNumberGenerator.new()
## The strength with which the camera will shake
var shake_strength: float = 0.0
## The amount of distance the camera will pan
var pan_strength: float = 0.0
## Timer that limits the increase in shake per unit of time
var shake_timer: Timer = Timer.new()


func _ready() -> void:
	pan_strength = max_pan_strength
	#EventBus._on_player_charging.connect(func(): pan_strength = max_pan_strength)
	#EventBus._on_player_shot.connect(func(): pan_strength = 0)
	EventBus._on_player_charging.connect(func(): shake_timer.start())
	EventBus._on_player_shot.connect(reset_shake)
	
	shake_timer.wait_time = 1
	shake_timer.one_shot = false
	shake_timer.timeout.connect(func(): shake_strength = min(max_shake_strength, shake_strength + shake_strength_increase))
	add_child(shake_timer)

func reset_shake():
	shake_strength = 0
	shake_timer.stop()

func _process(_delta: float) -> void:
	if shake_strength > 0:
		apply_camera_shake()
	
	#if pan_strength > 0 and shake_timer.is_stopped():
	#	shake_timer.start()
	#if pan_strength == 0:
	#	shake_strength = 0
	
	apply_camera_offset()


## Applies a camera shake to the camera's offset
func apply_camera_shake() -> void:
	offset = random_offset()


## Applies an offset to the camera's position to interpolate towards the mouse position
func apply_camera_offset() -> void:
	var mouse_offset = (get_viewport().get_mouse_position() - get_viewport_rect().size / 2)
	position = lerp(Vector2(), mouse_offset.normalized() * pan_strength, mouse_offset.length() / 1000)


## Provides a random direction and strength vector2
func random_offset() -> Vector2:
	return Vector2(rng.randf_range(-shake_strength, shake_strength), rng.randf_range(-shake_strength, shake_strength))
