@tool
class_name DestructablePlatform
extends StaticBody2D

@export var texture: Texture2D
@export var collision_shape_size: Vector2
@export var destroy_area_size: Vector2
@export var destroy_area_offset: Vector2
@export var destroy_time: float

@onready var sprite: Sprite2D = $Sprite2D
@onready var collision_shape: CollisionShape2D = $CollisionShape2D
@onready var destroy_area: Area2D = $DestroyArea
@onready var destroy_area_collision_shape: CollisionShape2D = $DestroyArea/CollisionShape2D
@onready var flash_player: AnimationPlayer = $Flash

var destroy_timer: Timer = Timer.new()

func _ready() -> void:
	destroy_timer.wait_time = destroy_time
	destroy_timer.timeout.connect(func(): queue_free())
	add_child(destroy_timer)
	sprite.texture = texture
	collision_shape.shape.size = collision_shape_size
	destroy_area_collision_shape.shape.size = destroy_area_size
	
	var animation = flash_player.get_animation("one_third")
	animation.clear()
	var track_key = animation.add_track(Animation.TYPE_VALUE)
	animation.track_set_path(track_key, "Sprite2D:self_modulate")
	animation.track_insert_key(track_key, 0.0, Color(1, 1, 1, 0.7))
	animation.track_insert_key(track_key, (destroy_time / 3) / 10, Color(1, 1, 1, 0.4))
	animation.track_insert_key(track_key, ((2 * destroy_time) / 3) / 10, Color(1, 1, 1, 0.7))
	animation.length = ((2 * destroy_time) / 3) / 10
	animation.loop_mode = Animation.LOOP_LINEAR
	
	animation = flash_player.get_animation("two_thirds")
	animation.clear()
	track_key = animation.add_track(Animation.TYPE_VALUE)
	animation.track_set_path(track_key, "Sprite2D:self_modulate")
	animation.track_insert_key(track_key, 0.0, Color(1, 1, 1, 1.0))
	animation.track_insert_key(track_key, ((2 * destroy_time) / 3) / 10, Color(1, 1, 1, 0.7))
	animation.track_insert_key(track_key, ((4 * destroy_time) / 3) / 10, Color(1, 1, 1, 1.0))
	animation.length = ((4 * destroy_time) / 3) / 10
	animation.loop_mode = Animation.LOOP_LINEAR


func _physics_process(_delta: float) -> void:
	sprite.texture = texture
	collision_shape.shape.size = collision_shape_size
	destroy_area_collision_shape.shape.size = destroy_area_size
	destroy_area.position = destroy_area_offset
	
	if not destroy_timer.is_stopped():
		if destroy_timer.time_left <= destroy_timer.wait_time / 3.0:
			flash_player.play("one_third")
		elif destroy_timer.time_left <= 2 * destroy_timer.wait_time / 3.0:
			flash_player.play("two_thirds")
		else:
			flash_player.play("three_thirds")


func _on_destroy_area_body_entered(_body) -> void:
	destroy_timer.start()
