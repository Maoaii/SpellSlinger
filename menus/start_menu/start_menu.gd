extends Control

@onready var resume_button: Button = $OuterContainer/InnerContainer/ButtonContainer/ResumeButton
@onready var new_game_button: Button = $OuterContainer/InnerContainer/ButtonContainer/ResumeButton

func _ready() -> void:
	resume_button.visible = SaveSystem.has(Global.save_1_name)
	if resume_button.visible:
		resume_button.grab_focus()
	else:
		new_game_button.grab_focus()
