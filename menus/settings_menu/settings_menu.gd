extends CanvasLayer

@onready var master_volume_slider: HSlider = $OuterContainer/InnerContainer/SettingsContainer/MasterVolumeContainer/MasterVolumeSlider
@onready var music_volume_slider: HSlider = $OuterContainer/InnerContainer/SettingsContainer/MusicVolumeContainer/MusicVolumeSlider
@onready var sfx_volume_slider: HSlider = $OuterContainer/InnerContainer/SettingsContainer/SFXContainer/SFXVolumeSlider
@onready var fullscreen_checkbox: CheckBox = $OuterContainer/InnerContainer/SettingsContainer/FullscreenContainer/FullscreenCheckbox

func _ready() -> void:
	if SaveSystem.has("save_1"):
		master_volume_slider.set_value_no_signal(SaveSystem.get_var("save_1:master_volume"))
		music_volume_slider.set_value_no_signal(SaveSystem.get_var("save_1:music_volume"))
		sfx_volume_slider.set_value_no_signal(SaveSystem.get_var("save_1:sfx_volume"))
		fullscreen_checkbox.button_pressed = SaveSystem.get_var("save_1:fullscreen")
	else:
		master_volume_slider.set_value_no_signal(1.0)
		music_volume_slider.set_value_no_signal(1.0)
		sfx_volume_slider.set_value_no_signal(1.0)
		fullscreen_checkbox.button_pressed = false
	
	fullscreen_checkbox.grab_focus()

func _on_back_button_delete_signal() -> void:
	queue_free()


func _on_master_volume_slider_value_changed(value: float) -> void:
	SaveSystem.set_var("save_1:master_volume", value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear_to_db(value))


func _on_music_volume_slider_value_changed(value: float) -> void:
	SaveSystem.set_var("save_1:music_volume", value)
	SoundManager.set_music_volume(value)


func _on_sfx_volume_slider_value_changed(value: float) -> void:
	SaveSystem.set_var("save_1:sfx_volume", value)
	SoundManager.set_sound_volume(value)
	SoundManager.set_ambient_sound_volume(value)


func _on_fullscreen_checkbox_toggled(toggled_on: bool) -> void:
	SaveSystem.set_var("save_1:fullscreen", toggled_on)
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN if toggled_on else DisplayServer.WINDOW_MODE_WINDOWED)
