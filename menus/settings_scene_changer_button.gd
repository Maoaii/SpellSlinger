extends Button

signal delete_signal()

@export var scene: String
@export var fade_out_speed: float = 0.3
@export var fade_in_speed: float = 0.3
@export var fade_out_pattern: String = "fade"
@export var fade_in_pattern: String = "fade"
@export var fade_out_smoothness = 0.1 # (float, 0, 1)
@export var fade_in_smoothness = 0.1 # (float, 0, 1)
@export var fade_out_inverted: bool = false
@export var fade_in_inverted: bool = false
@export var color: Color = Color(0, 0, 0)
@export var timeout: float = 0.0
@export var clickable: bool = false
@export var add_to_back: bool = true

@onready var fade_out_options = SceneManager.create_options(fade_out_speed, fade_out_pattern, fade_out_smoothness, fade_out_inverted)
@onready var fade_in_options = SceneManager.create_options(fade_in_speed, fade_in_pattern, fade_in_smoothness, fade_in_inverted)
@onready var general_options = SceneManager.create_general_options(color, timeout, clickable, add_to_back)

func _ready() -> void:
	var fade_in_first_scene_options = SceneManager.create_options(1, "fade")
	var first_scene_general_options = SceneManager.create_general_options(Color(0, 0, 0), 1, false)
	SceneManager.show_first_scene(fade_in_first_scene_options, first_scene_general_options)
	# code breaks if scene is not recognizable
	SceneManager.validate_scene(scene)
	# code breaks if pattern is not recognizable
	SceneManager.validate_pattern(fade_out_pattern)
	SceneManager.validate_pattern(fade_in_pattern)
	
	self.pressed.connect(_on_button_pressed)

func _on_button_pressed():
	if scene == "back":
		SceneManager.fade_out_finished.connect(remove_settings)
		SceneManager.change_scene("ignore", fade_out_options, fade_in_options, general_options)
	elif scene == "settings_menu":
		SceneManager.fade_out_finished.connect(instantiate_settings)
		SceneManager.change_scene("null", fade_out_options, fade_in_options, general_options)

func instantiate_settings():
	var settings_menu = SceneManager.create_scene_instance("settings_menu")
	get_tree().root.add_child(settings_menu)
	SceneManager.fade_out_finished.disconnect(instantiate_settings)

func remove_settings():
	delete_signal.emit()
	SceneManager.fade_out_finished.disconnect(remove_settings)
