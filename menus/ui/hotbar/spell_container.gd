class_name SpellContainer
extends PanelContainer

@export var spell: SpellResource

@onready var spell_icon: TextureRect = $MarginContainer/SpellIcon

func _ready() -> void:
	spell_icon.texture = spell.icon
