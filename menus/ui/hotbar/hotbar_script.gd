class_name Hotbar
extends HBoxContainer

var spell_containers: Array[SpellContainer]
var spell_timers: Array[Timer]

func _ready() -> void:
	select_spell(-1)
	EventBus._on_spell_selected.connect(select_spell)
	EventBus._on_spell_cast.connect(select_spell)

func select_spell(index: int):
	for i in range(spell_containers.size()):
		if not spell_timers[i].is_stopped():
			spell_containers[i].modulate.a = 0.3
		elif index == i:
			spell_containers[i].modulate.a = 1
		else:
			spell_containers[i].modulate.a = 0.7

func update_spell_containers(spells: Array[SpellResource]) -> void:
	spell_containers.clear()
	spell_timers.clear()
	
	for i in range(spells.size()):
		var spell_container_scene = preload("res://menus/ui/hotbar/spell_container.tscn")
		var spell_container = spell_container_scene.instantiate()
		spell_container.spell = spells[i]
		
		spell_containers.append(spell_container)
		add_child(spell_container)
		
		var timer: Timer = Timer.new()
		timer.name = "Spell" + str(i + 1) + " Cooldown Timer"
		timer.one_shot = true
		timer.wait_time = spells[i].spell_stats.cooldown_time
		timer.timeout.connect(func(): spell_containers[i].modulate.a = 0.7 )
		spell_timers.append(timer)
		add_child(timer)

func start_casting_spell(index: int) -> void:
	spell_timers[index].start()

func is_spell_ready(index: int) -> bool:
	return index in range(0, spell_containers.size()) && spell_timers[index].is_stopped() 
