extends CanvasLayer

@onready var resume_button: Button = $OuterContainer/InnerContainer/ButtonContainer/ResumeButton

func _ready() -> void:
	hide()

func _unhandled_input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = not get_tree().paused
		visible = not visible

func _on_resume_button_pressed():
	get_tree().paused = false
	hide()

func _on_visibility_changed():
	if visible:
		resume_button.grab_focus()
		Global.set_cursor(Global.CURSOR.MENU)
	else:
		Global.set_cursor(Global.CURSOR.GAMEPLAY_IDLE)
