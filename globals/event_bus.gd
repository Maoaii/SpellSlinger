## Declare signals here that should be accessed by multiple entities
## in different scopes.
extends Node

## Signal fired whenever a screen shake is necessary.
## For example, when holding a shooting charge for too long.
signal _on_screen_shake(strength: float)

## Signal fired whenever the player starts charging a shot
signal _on_player_charging()
## Signal fired whenever the player shoots
signal _on_player_shot()

signal _on_player_dashing()
signal _on_player_stopped_dashing()

signal _on_spell_selected(index: int)
signal _on_spell_cast()

signal disable_player_input(time: float)
