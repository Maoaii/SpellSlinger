extends Node

enum CURSOR {
	MENU,
	GAMEPLAY_IDLE,
	CHARGING
}

var cursor_texture = {
	CURSOR.MENU: load("res://cursors/Cursor Default.png"),
	CURSOR.GAMEPLAY_IDLE: load("res://cursors/Cursor Target Move B.png"),
	CURSOR.CHARGING: load("res://cursors/Cursor Target Move A.png"),
}

## Name of the first save file
var save_1_name: String = "save_1"

var current_cursor: CURSOR

func _ready() -> void:
	validate_save_file()
	
	setup_settings()
	
	EventBus._on_player_charging.connect(func(): set_cursor(CURSOR.CHARGING))
	EventBus._on_player_shot.connect(func(): set_cursor(CURSOR.GAMEPLAY_IDLE))

func set_cursor(new_cursor: CURSOR):
	Input.set_custom_mouse_cursor(cursor_texture[new_cursor], Input.CURSOR_ARROW, Vector2(0, 0) if new_cursor == CURSOR.MENU else Vector2(10, 10))

func get_cursor() -> CURSOR:
	return current_cursor

## Checks if there is a save file available.
## If there isn't, creates a new one with default values and saves it.
func validate_save_file():
	if not SaveSystem.has("save_1"):
		SaveSystem.set_var("save_1", SaveFile.new())
		return
	
	var save_file: Dictionary = SaveSystem.get_var("save_1")
	var mock_save_file = SaveFile.new()
	
	if not save_file.has_all(mock_save_file.variables):
		SaveSystem.set_var("save_1", SaveFile.new())
		return


## Sets up the settings values according to the save file
func setup_settings():
	var fullscreen: bool = SaveSystem.get_var("save_1:fullscreen")
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN if fullscreen else DisplayServer.WINDOW_MODE_WINDOWED)
	
	setup_audio_bus()


## Sets up the audio buses volume levels according to the save file
func setup_audio_bus():
	var save_file: Dictionary = SaveSystem.get_var("save_1")
	
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear_to_db(save_file.get("master_volume")))
	SoundManager.set_sound_volume(save_file.get("sfx_volume"))
	SoundManager.set_ambient_sound_volume(save_file.get("sfx_volume"))
	SoundManager.set_music_volume(save_file.get("music_volume"))
