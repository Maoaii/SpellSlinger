class_name SaveFile
extends Resource

var variables: Array = [
	"fullscreen",
	"master_volume",
	"music_volume",
	"sfx_volume",
	"health",
]

@export_category("Settings")
@export var fullscreen: bool = false
@export var master_volume: float = 1.0
@export var music_volume: float = 1.0
@export var sfx_volume: float = 1.0

@export_category("Player Stats")
@export var health: int = 5
